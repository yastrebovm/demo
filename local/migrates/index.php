<?
require_once $_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php";
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

Loader::includeModule("highloadblock");
$obUserField  = new CUserTypeEntity;
// Проверка на существование HL с именем Catalog
$dbHblock = HL\HighloadBlockTable::getList(array("filter"=>array("NAME"=>"Catalog")));
if(!$dbHblock->fetch())
{
    // Если нету то создадим его
    $dataParam = array("NAME" => "Catalog", "TABLE_NAME" => "catalog");
    $result = HL\HighloadBlockTable::add($dataParam);
    $idCatalog = $result->getId();

    $arUserFields = array (
        array (
            'ENTITY_ID' => 'HLBLOCK_'.$idCatalog,
            'FIELD_NAME' => 'UF_NAME',
            'USER_TYPE_ID' => 'string',
            'XML_ID' => 'UF_NAME',
            'SORT' => '200',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
        )
    );



    foreach($arUserFields as $arFields){
        $dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => $arFields["ENTITY_ID"], "FIELD_NAME" => $arFields["FIELD_NAME"]));
        if($dbRes->Fetch()){
            continue;
        }

        $ID_USER_FIELD = $obUserField->Add($arFields);
    }
}

// Проверка на существование HL с именем ProductAssign
$dbHblock = HL\HighloadBlockTable::getList(array("filter"=>array("NAME"=>"ProductAssign")));
if(!$dbHblock->fetch())
{
    // Если нету то создадим его
    $dataParam = array("NAME" => "ProductAssign", "TABLE_NAME" => "product_assign");
    $result = HL\HighloadBlockTable::add($dataParam);
    $idProductAssign = $result->getId();

    $arUserFields = array();
    $arUserFields = array (
        //PRODUCT_ID - привязка к товару
        array (
            'ENTITY_ID' => 'HLBLOCK_'.$idProductAssign,
            'FIELD_NAME' => 'UF_PRODUCT_ID',
            'USER_TYPE_ID' => 'iblock_element',
            'XML_ID' => 'UF_PRODUCT_ID',
            'SORT' => '200',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS'          => array(
                'DISPLAY' => 'LIST',
                'LIST_HEIGHT' => 5,
                'IBLOCK_ID' => 1,
            ),
            /* Подпись в форме редактирования */
            'EDIT_FORM_LABEL'   => array(
                'ru'    => 'привязка к товару',
                'en'    => 'привязка к товару',
            ),
            /* Заголовок в списке */
            'LIST_COLUMN_LABEL' => array(
                'ru'    => 'привязка к товару',
                'en'    => 'привязка к товару',
            ),
            /* Подпись фильтра в списке */
            'LIST_FILTER_LABEL' => array(
                'ru'    => 'привязка к товару',
                'en'    => 'привязка к товару',
            ),
        ),
        //CATALOG_ID - привязка к созданной ранее сущности Catalog
        array (
            'ENTITY_ID' => 'HLBLOCK_'.$idProductAssign,
            'FIELD_NAME' => 'UF_CATALOG_ID',
            'USER_TYPE_ID' => 'hlblock',
            'XML_ID' => 'UF_CATALOG_ID',
            'SORT' => '200',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS'          => array(
                'DISPLAY' => 'LIST',
                'LIST_HEIGHT' => 5,
                'HLBLOCK_ID' => $idCatalog,
                'HLFIELD_ID' => 0
            ),
            /* Подпись в форме редактирования */
            'EDIT_FORM_LABEL'   => array(
                'ru'    => 'привязка к созданной ранее сущности Catalog',
                'en'    => 'привязка к созданной ранее сущности Catalog',
            ),
            /* Заголовок в списке */
            'LIST_COLUMN_LABEL' => array(
                'ru'    => 'привязка к созданной ранее сущности Catalog',
                'en'    => 'привязка к созданной ранее сущности Catalog',
            ),
            /* Подпись фильтра в списке */
            'LIST_FILTER_LABEL' => array(
                'ru'    => 'привязка к созданной ранее сущности Catalog',
                'en'    => 'привязка к созданной ранее сущности Catalog',
            ),
        )
    );



    foreach($arUserFields as $arFields){
        $dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => $arFields["ENTITY_ID"], "FIELD_NAME" => $arFields["FIELD_NAME"]));
        if($dbRes->Fetch()){
            continue;
        }

        $ID_USER_FIELD = $obUserField->Add($arFields);
    }
}


// Проверка на существование HL с именем UsersAssign
$dbHblock = HL\HighloadBlockTable::getList(array("filter"=>array("NAME"=>"UsersAssign")));
if(!$dbHblock->fetch())
{
    // Если нету то создадим его
    $dataParam = array("NAME" => "UsersAssign", "TABLE_NAME" => "users_assign");
    $result = HL\HighloadBlockTable::add($dataParam);
    $idUsersAssign = $result->getId();

    $arUserFields = array();
    $arUserFields = array (
        //USER_ID  - привязка к товару
        array (
            'ENTITY_ID' => 'HLBLOCK_'.$idUsersAssign,
            'FIELD_NAME' => 'UF_USER_ID',
            'USER_TYPE_ID' => 'double',
            'XML_ID' => 'UF_USER_ID',
            'SORT' => '200',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS'          => array(
                'DISPLAY' => 'LIST',
                'LIST_HEIGHT' => 5,
                'IBLOCK_ID' => 1,
            ),
            /* Подпись в форме редактирования */
                'EDIT_FORM_LABEL'   => array(
                    'ru'    => 'привязка к пользователю',
                    'en'    => 'привязка к пользователю',
                ),
            /* Заголовок в списке */
                'LIST_COLUMN_LABEL' => array(
                    'ru'    => 'привязка к пользователю',
                    'en'    => 'привязка к пользователю',
                ),
            /* Подпись фильтра в списке */
                'LIST_FILTER_LABEL' => array(
                    'ru'    => 'привязка к пользователю',
                    'en'    => 'привязка к пользователю',
                ),
        ),
        //CATALOG_ID - привязка к созданной ранее сущности Catalog
        array (
            'ENTITY_ID' => 'HLBLOCK_'.$idUsersAssign,
            'FIELD_NAME' => 'UF_CATALOG_ID',
            'USER_TYPE_ID' => 'hlblock',
            'XML_ID' => 'UF_CATALOG_ID',
            'SORT' => '200',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS'          => array(
                'DISPLAY' => 'LIST',
                'LIST_HEIGHT' => 5,
                'HLBLOCK_ID' => $idCatalog,
                'HLFIELD_ID' => 0
            ),
            /* Подпись в форме редактирования */
            'EDIT_FORM_LABEL'   => array(
                'ru'    => 'привязка к созданной ранее сущности Catalog',
                'en'    => 'привязка к созданной ранее сущности Catalog',
            ),
            /* Заголовок в списке */
            'LIST_COLUMN_LABEL' => array(
                'ru'    => 'привязка к созданной ранее сущности Catalog',
                'en'    => 'привязка к созданной ранее сущности Catalog',
            ),
            /* Подпись фильтра в списке */
            'LIST_FILTER_LABEL' => array(
                'ru'    => 'привязка к созданной ранее сущности Catalog',
                'en'    => 'привязка к созданной ранее сущности Catalog',
            ),
        )
    );



    foreach($arUserFields as $arFields){
        $dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => $arFields["ENTITY_ID"], "FIELD_NAME" => $arFields["FIELD_NAME"]));
        if($dbRes->Fetch()){
            continue;
        }

        $ID_USER_FIELD = $obUserField->Add($arFields);
    }
}
