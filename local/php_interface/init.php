<?
class GeoIpService {

    private $arParams = array();
    public $arResult = array();

    function __construct($strIP = '')
    {
        // Если явно указан IP
        if($strIP != '')
        {
            $this->validateIP($strIP);
            $this->arParams = array('IPAddress' => $strIP);
        }
        else
        {
            // Если не указан берет текущий
            $this->arParams = array('IPAddress' => $this->getRealIpAddr());
        }

        $this->getRequest();
        return $this->arResult;
    }


    // Определение IP
    public function getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    // Валидация IP
    private function validateIP($ip)
    {
        if (!preg_match_all('((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)',$ip))
        {
            throw new RuntimeException('invalid IP adres');
        }
    }

    // Запрос в сервису
    private function getRequest()
    {
        $client = new SoapClient("http://www.webservicex.net/geoipservice.asmx?WSDL");

        $result = $client->GetGeoIP($this->arParams)->GetGeoIPResult->ReturnCodeDetails;
        if($result == 'Success')
        {
            $info = $client->GetGeoIP($this->arParams)->GetGeoIPResult;

            $this->arResult['IP'] = $info->IP;
            $this->arResult['CountryCode'] = $info->ReturnCode;
            $this->arResult['CountryName'] = $info->CountryName;
        } else {
            if (!preg_match_all('((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)',$ip))
            {
                throw new RuntimeException($result);
            }
        }
    }

}